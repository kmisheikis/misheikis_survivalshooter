﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;          //the position that the camera will be following 
    public float smoothing = 5f;      //the speed with which the camera will be following 

    Vector3 offset;                   //the initial offset from the target

    private void Start()
    {
        //calculate the initial offset
        offset = transform.position - target.position;
    }

    private void FixedUpdate()
    {
        //create a position the camera is aiming for based on the offset from the target 
        Vector3 targetCamPos = target.position + offset;
        //smoothly interpolate between the camera's current position and it's target position
        transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing * Time.deltaTime);
    }
}


