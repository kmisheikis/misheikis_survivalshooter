﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ScoreMultiplierManager : MonoBehaviour
{
    public int scoreMultiplier = 0;
    public Text text;
    public float lastUpdate;

    void Awake()
    {
        text = GetComponent<Text>();
        scoreMultiplier = 0;
    }
    void Update()
    {
        text.text = scoreMultiplier + "x Points";
       

        if (Time.timeSinceLevelLoad - lastUpdate >= 30)
        {
            scoreMultiplier = scoreMultiplier + 2;
            lastUpdate = Time.timeSinceLevelLoad;
        }

        print(scoreMultiplier);
    }
}

