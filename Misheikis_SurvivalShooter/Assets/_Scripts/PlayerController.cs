﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;



public class PlayerController : MonoBehaviour
{
    public float speed;
    public Text countText;
    public Text NiceJobText;
    bool onGround;
    public float jumpPower;
    public int count;
    public Text RestartText;





    Rigidbody rb;
    private void Update()
    {
        //Check if ball is on top of surface using raycast
        onGround = Physics.Raycast(transform.position, Vector3.down, .65f); //.55f
        if (Input.GetKey(KeyCode.Space) && onGround)
        {
            Jump();
        }
        if (Input.GetKeyDown(KeyCode.R))
            //If the R button is pressed
        {
            Debug.Log("R key was pressed");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex -1);
            //This says that everytime the player presses the R key it loads the main menu of the game
        }

        if (Input.GetKeyUp(KeyCode.R))
        {
            Debug.Log("R key was released");
            //This is just for me to know that the script worked
        }

    }
       


    void Jump()
    {
        //add upward force to make character jump
        rb.AddForce(Vector3.up * jumpPower);

    }


   

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        NiceJobText.text = "";
        RestartText.text = "";
        
       
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        //This is referencing the horizontal and vertical movment of the player

        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);

        rb.AddForce(movement * speed);
        //This is calculating the velocity of the player

        if (rb.position.y < -1f)
        {
            FindObjectOfType<GameManager>().EndGame();
        }
    }
    void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Pick Up"))
            {
            other.gameObject.SetActive (false);
            count = count + 1;
            SetCountText();
            
            }

        }
    void SetCountText ()
    {
        countText.text = "Count " + count.ToString();
        if (count >= 89) 
        {
            NiceJobText.text = "NICE JOB!";
            //This display the text NICE JOB! to the player when they collect all 89 of the cubes
            RestartText.text = "HIT 'R' TO RESTART";
            //This shows the restart button text when the player has collected all of the squares instead of at the beginning
        }

   
     }
        
}
//scales player everytime they pick up an object 
//transform.localscale += new Vectro 3(.1f, .1f, .1f);

//increases speed everytime a cube is picked up 
//speed += 1f;
 