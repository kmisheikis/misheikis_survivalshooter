﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour
{
    public static int score;
    public static float multiplier = 2.0f;
    public float lastTime; 
   


    Text text;


    void Awake ()
    {
        text = GetComponent <Text> ();
        score = 0;
    }


    void Update ()
    {
        print("SCORE:" + score);
        text.text = "Score: " + score;

        if (Time.timeSinceLevelLoad + lastTime >= 30)
        {
            lastTime = Time.timeSinceLevelLoad;
            multiplier = multiplier + 2f;
        }

    }
}




    
