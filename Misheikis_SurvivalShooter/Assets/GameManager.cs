﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    bool GameHasEnded = false;
    public float restartDelay;
    public void EndGame()
        //This makes the endgame scenario into a true or false statement making it easy to tell the game that it has ended
    {
        if (GameHasEnded == false)
        {
            GameHasEnded = true;
            Debug.Log("Game Over");
            Invoke("Restart", restartDelay);
            //This tells the game that the game has ended
        }

    }
    void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        //This reloads the active scene whenever the player falls off of the level

    }
}