﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 6f;       //sets the player speed
    Vector3 movement;              //stores movement we want to apply to the player
    Animator anim;                 //reference to animator component
    Rigidbody playerRigidbody;     //reference to rigid body component 
    int floorMask;                 // layer mask so that a ray can be cast just at gameobjects on the floor
    float camRayLength = 100f;     //length of the ray from the camera into the scene

    private void Awake()
    {
        //created a layer mask for the floor layer
        floorMask = LayerMask.GetMask("Floor");

        //set up references
        anim = GetComponent<Animator>();
        playerRigidbody = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        //stores the input axes
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        //moves the player around the scene
        Move(h, v);

        //turns the player to face the mouse cursor
        Turning();

        //animates the player
        Animating (h, v); 
    }

    void Move (float h, float v)
    {
        //sets the movement vector based on the axis input 
        movement.Set(h, 0f, v);

        //normalizes the movement vector and make it proportional to the speed per second
        movement = movement.normalized * speed * Time.deltaTime;

        //moves the pkayer to it's current position plus the movment 
        playerRigidbody.MovePosition(transform.position + movement);
    }

    void Turning ()
    {
        //create a ray from the mouse cursor on screen in the direction of the camera
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        //create a RaycastHit variable to store information about what was hit by the ray
        RaycastHit floorHit;

        //perform the raycast and if it hits something on the floor layer...
        if(Physics.Raycast (camRay, out floorHit, camRayLength, floorMask))
        {
            //create a vector from the player to the ppoint on the floor that raycast from the mouse hit 
            Vector3 playerToMouse = floorHit.point - transform.position;

            //ensure the vector is entirely along the floor plane 
            playerToMouse.y = 0f;

            //create a quaternion (rotation) based on looking down the vector from the player to the mouse 
            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);

            //Set the player's rotation to this new rotation
            playerRigidbody.MoveRotation (newRotation);
        }
    }

    void Animating (float h, float v)
    {
        //create a boolean that is true if either of the input axes is non-zero
        bool walking = h != 0f || v != 0f;

        //Tell the animator whether or not the player is walking
        anim.SetBool("IsWalking", walking);
    }

        
        ///ended the video 21:41    
}



