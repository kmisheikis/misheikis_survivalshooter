﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public Text gameTimerText;
    //We first have to establish the text so we can reference it later in the code
    float gameTimer = 0f;
    //This sets the game timer to 0 to make sure that it doesn't start with any other number other than 0
    public PlayerController playerController;
    //This references back to the player controller script to figure out how many cubes have been collected
   

    // Start is called before the first frame update
    void Start()
    {
        
        
    }

    // Update is called once per frame
    void Update()
    {
        if (playerController.count <= 89)
        {
            gameTimer += Time.deltaTime;
         

            int seconds = (int)(gameTimer % 60);
            int minutes = (int)(gameTimer / 60) % 60;
            int hours = (int)(gameTimer / 3600) % 24;
            //This sets the values of the individual number shown on the UI text

            string timerString = string.Format("{0:0}:{1:00}:{2:00}", hours, minutes, seconds);
            //This formats the UI text to show up like a regular stopwatch (0:00:00)

            gameTimerText.text = timerString;
        }
        //if (count >=89)
        {
           // keepTiming = false;
           // gameTimer = Time.timeSinceLevelLoad;
        }
    }
}

 

